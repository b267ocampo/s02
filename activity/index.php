<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Activity 2</title>
</head>
<body>

	<h1>Divisible of Five</h1>
	<p><?php echo printDivisibleOfFive(); ?></p>

	<h1>Array Manipulation</h1>
	<?php 
		array_push($students, 'John Smith'); 
		print_r($students);
		echo '</br> </br>' . count($students);

		array_push($students, 'Jane Smith');
		echo '</br> </br>';
		print_r($students);
		echo '</br> </br>' . count($students);

		array_shift($students);
		echo '</br> </br>';
		print_r($students);
		echo '</br> </br>' . count($students);
	?>

	
</body>
</html>